<?php
session_start();

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "testdb";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if (!$conn) 
{
    die("Connection failed: " . mysqli_connect_error());
}



//set validation error flag as false
$error = false;

//check if form is submitted
if (isset($_POST['signup'])) {
    $textfield = mysqli_real_escape_string($conn, $_POST['name']);
    $checkBox = array("items" => $_POST["items"]); 
    $dropDown = $_POST["items_dropdown"];
    $items_dropdown = implode(',', $checkBox["items"]);
    
    
    if (!$error) {
        if(mysqli_query($conn, "INSERT INTO sample(first_val,second_val,third_val) VALUES('" . $textfield . "', '" . $items_dropdown . "', '" . $dropDown . "')")) {
            $successmsg = "Successfully inserted!";
        } else {
            $errormsg = "Error in inserting data...Please try again later!";
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Sample Insertion Script</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" >
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 well">
            <form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="insertform">
                <fieldset>
                    <legend>Give Values</legend>

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" placeholder="Enter Full Name" required class="form-control" />
                       
                    </div>
                    
                    <div class="form-group">
                        <label for="name">select items</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" name="items[]" value="Item1" class="form-check-input">
                                        Item 1
                                </label>
                                &nbsp;&nbsp;
                                <label class="form-check-label">
                                    <input type="checkbox" name="items[]" value="Item2" class="form-check-input">
                                        Item 2
                                </label>
                                &nbsp;&nbsp;
                                <label class="form-check-label">
                                    <input type="checkbox" name="items[]" value="Item3" class="form-check-input">
                                        Item 3
                                </label>
                                &nbsp;&nbsp;
                                <label class="form-check-label">
                                    <input type="checkbox" name="items[]" value="Item4" class="form-check-input">
                                        Item 4
                                </label>
                            </div>
                    </div>

                    <div class="form-group">
                        <label for="name">select item 2</label>
       

<select class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" name="items_dropdown">
  <option value="Volvo">Volvo</option>
  <option value="Saab">Saab</option>
  <option value="Opel">Opel</option>
  <option value="Audi">Audi</option>
</select>
                    </div>

                 
                    <div class="form-group">
                        <input type="submit" name="signup" value="Sign Up" class="btn btn-primary" />
                    </div>
                </fieldset>
            </form>
            <span class="text-success"><?php if (isset($successmsg)) { echo $successmsg; } ?></span>
            <span class="text-danger"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
        </div>
    </div>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>